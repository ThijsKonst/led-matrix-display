#include <FastLED.h>
#include <EtherCard.h>
#include <ctime>
#include <string>
#include <sstream>

// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Wire.h>
#include "RTClib.h"
RTC_DS1307 rtc;

#define LED_PIN     3
#define NUM_LEDS    115
#define BRIGHTNESS  255
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

const String charIndex = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789?!:\\";
byte charArray[41][4] = {
  {0b01111001, 0b10011111, 0b10010000, 4},
  {0b11101001, 0b11101001, 0b11100000, 4},
  {0b01101001, 0b10001001, 0b01100000, 4},
  {0b11101001, 0b10011001, 0b11100000, 4},
  {0b11111000, 0b11101000, 0b11110000, 4},
  {0b11111000, 0b11101000, 0b10000000, 4},
  {0b01111000, 0b10111001, 0b01110000, 4},
  {0b10011001, 0b11111001, 0b10010000, 4},
  {0b11100100, 0b01000100, 0b11100000, 4},
  {0b01110001, 0b00011001, 0b01100000, 4},
  {0b10011010, 0b11001010, 0b10010000, 4},
  {0b10001000, 0b10001000, 0b11110000, 4},
  {0b10011111, 0b10011001, 0b10010000, 4},
  {0b10011101, 0b10111001, 0b10010000, 4},
  {0b01101001, 0b10011001, 0b01100000, 4},
  {0b11101001, 0b11101000, 0b10000000, 4},
  {0b01101001, 0b10011011, 0b01110000, 4},
  {0b11101001, 0b11101010, 0b10010000, 4},
  {0b01111000, 0b01100001, 0b11100000, 4},
  {0b11100100, 0b01000100, 0b01000000, 4},
  {0b10011001, 0b10011001, 0b01100000, 4},
  {0b10101010, 0b10101010, 0b01000000, 4},
  {0b10011001, 0b10011111, 0b10010000, 4},
  {0b10011001, 0b01101001, 0b10010000, 4},
  {0b10101010, 0b01000100, 0b01000000, 4},
  {0b11110001, 0b01101000, 0b11110000, 4},
  {0,0,0, 4},
  {0b01101011, 0b11011001, 0b01100000, 4},
  {0b01001100, 0b01000100, 0b01000000, 2},
  {0b01101001, 0b00100100, 0b11110000, 4},
  {0b11100001, 0b01100001, 0b11100000, 4},
  {0b10011001, 0b11110001, 0b00010000, 4},
  {0b11111000, 0b11100001, 0b11100000, 4},
  {0b01101000, 0b11101001, 0b01100000, 4},
  {0b11110001, 0b00100010, 0b00100000, 4},
  {0b01101001, 0b01101001, 0b01100000, 4},
  {0b01101001, 0b01110001, 0b01100000, 4},
  {0b11000010, 0b01000000, 0b01000000, 3},
  {0b10001000, 0b10000000, 0b10000000, 1},
  {0b00001000, 0b00001000, 0b00000000, 1},
  {0,0,0,1}
};

TBlendType currentBlending;
uint8_t colorIndex;
byte Ethernet::buffer[700];
static uint8_t mymac[] = { 0x32,0x29,0x55,0x98,0x14,0x65 };

void setup() {
    Serial.begin(9600);    
    delay( 3000 ); // power-up safety delay
    if (! rtc.begin()) {
      Serial.println("Couldn't find RTC");
      while (1);
    }
    if (! rtc.isrunning()) {
      Serial.println("RTC is NOT running!");
      // following line sets the RTC to the date & time this sketch was compiled
      rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    }
    if (ether.begin(sizeof Ethernet::buffer, mymac, SS) == 0){
      Serial.println(F("Failed to access Ethernet controller"));
    }
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
    currentBlending = LINEARBLEND;
    colorIndex = 0;
    scrollText("Getting IP", 20);
    

}

void loop() {
  Serial.println("Loop start");
  // put your main code here, to run repeatedly:
  scrollText("QUIRIJN KOM BORRELEN MAARTEN LEKKER BEZIG MARISKA LAF KOM BORRELEN", 20);
  for (int i = 0; i < 10; i++){
    DateTime now = rtc.now();

    int delta = (10 - now.daysOfTheWeek()) % 7;
    if (now.hour() >= 20) {
      delta = (9 - now.daysOfTheWeek()) % 7;
    }
    int delta_hour = (43 - now.hour()) % 24;

    std::ostringstream oss;
    if (delta_hour == 0 && delta == 0){
      int delta_min = (59 - now.minute()) % 60;
      int delta_sec = (59 - now.second()) % 60;
      if (delta_min < 10){
        oss << '0';
      }
      oss << delta_min << ':';
      if (delta_sec < 10) {
        oss << '0';
      }
      oss << delta_sec;
    } else if (delta == 0){
      oss << delta_hour << "uur";
    } else {
      oss << delta << "dag";
    }
    std::string print = oss.str();

    displayString(print, 4);
    FastLED.show();
    delay(1000);
  }
}

int getLEDID(int x, int y){
  if ((x & 0x01) == 0){
    return (22-x) * 5 + y;
  } else {
    return (22-x) * 5 + (5-y) -1;
  }
}

void scrollText(String input, double delayValue){
  int offsets = input.length() * 5;
  int maxChars = 6;
  for (int i =0; i < input.length(); i++){
    for(int b =0; b < 5; b++){
      displayString(input.substring(i, input.length()), b);
      FastLED.show();
      delay(delayValue);
    }
  }
}

CRGB getColor(){
  return ColorFromPalette( PartyColors_p, colorIndex, BRIGHTNESS, currentBlending);
}

void displayString(String value, double offset){
  String toDisplay = value;
  int curOffset = offset;
  int curCol = 0;
  colorIndex += 1;
  for (int i=0;i<toDisplay.length();i++){
    byte curChar[4];
    memcpy(curChar, charArray[charIndex.indexOf(toDisplay.charAt(i))], 4);
    Serial.println(String(curChar[3]));
    for(int b=0; b<curChar[3]; b++){
      if (curOffset != 0){
        b = curOffset;
        curOffset = 0;
        if (b == curChar[3]){
          break;
        }
      }
      leds[getLEDID(curCol, 0)] = curChar[0] >> (7-b) & 0x01 ? getColor() : CRGB::Black;
      leds[getLEDID(curCol, 1)] = curChar[0] >> (3-b) & 0x01 ? getColor() : CRGB::Black;
      leds[getLEDID(curCol, 2)] = curChar[1] >> (7-b) & 0x01 ? getColor() : CRGB::Black;
      leds[getLEDID(curCol, 3)] = curChar[1] >> (3-b) & 0x01 ? getColor() : CRGB::Black;
      leds[getLEDID(curCol, 4)] = curChar[2] >> (7-b) & 0x01 ? getColor() : CRGB::Black;
      curCol += 1;
      if (curCol > 22){
        break;
      }
    }
    if (curCol < 23){
        setLedCol(curCol, CRGB::Black);
        curCol += 1;
    }
    if (curCol > 22){
      break;
    }
  }
  
}

void setLedCol(int col, CRGB colour) {
  for(int i = 0; i<5; i++){
    leds[getLEDID(col, i)] = colour;
  }
}
